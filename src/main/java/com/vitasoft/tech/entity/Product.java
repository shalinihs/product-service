package com.vitasoft.tech.entity;

import java.math.BigDecimal;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Document(value="product")
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class Product {
	@Id
	private String id;//unique
	private String name;
	private String discription;
	private BigDecimal price;
	

}
