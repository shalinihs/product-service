package com.vitasoft.tech.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.vitasoft.tech.entity.Product;

public interface ProductRepository extends MongoRepository<Product,String> {

}
